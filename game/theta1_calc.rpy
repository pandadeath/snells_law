
label theta1_calc:

    "All right, let\'s find the Angle of Incidence."
    hide instructor
    show whiteboard:
        zoom 0.5
        xpos 59 ypos 60     
    
    
    $good_theta2=False
    python:
        while not good_theta2:
            input_refracted=renpy.input("Please enter an refracted angle in degrees ")
            try:
                refracted_angle_number=float(input_refracted)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if 0< refracted_angle_number <90:
                    "stuff"
                    good_theta2=True
                else:
                    renpy.input("Please put in a number between 0 and 90\nPress Enter to try again.")
    
    $theta2=round(float(input_refracted),1)
    $rad_refracted=float(input_refracted)*math.pi/180
    "The Angle of Refraction is theta2=[theta2] degrees."
    
    show refracted:
        rotate 0
        xanchor 5 yanchor 200
        xpos 405 ypos 210
        
        #xpos 400-int(math.sqrt(x2_i)) ypos 300-int(math.sqrt(y2))
        transform_anchor True
        rotate_pad False
        rotate -float(input_refracted)-180

    show theta2 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[theta2]{/color}" at theta2_pos as line4
        
    "Choose the indices of refraction."
    
    $good_n1=False
    python:
        while not good_n1:
            n1=renpy.input("Please enter the incident medium\'s index of refraction")
            try:
                n1_number=float(n1)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if n1_number>=1:
                    "stuff"
                    good_n1=True
                else:
                    renpy.input("Light can\'t go faster than light.\nPut in  a number bigger than 1\nPress Enter to try again.")
    
    show n1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[n1]{/color}" at n1_pos as line2
    #$n2=renpy.input("The second index of refraction is:")
    
    $good_n2=False
    python:
        while not good_n2:
            n2=renpy.input("Please enter the refracted medium\'s index of refraction")
            try:
                n2_number=float(n2)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if n2_number>=1:
                    "stuff"
                    good_n2=True
                else:
                    renpy.input("Light can\'t go faster than light.\nPut in  a number bigger than 1\nPress Enter to try again.")
    
    show n2 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[n2]{/color}" at n2_pos as line3
    
    $n_ratio_b=float(n2)/float(n1)
    $sin_refracted=float(math.sin(rad_refracted))
    
    
    if sin_refracted*n_ratio_b>=1:
        $angle_i=90
        $angle_rad_i=math.pi/2
    else:
        python:
            angle_rad_i=math.asin(n_ratio_b*sin_refracted)
            angle_i=float(angle_rad_i)*180/math.pi
    $theta1=round(float(angle_i),1)
    $y1_i=float(200*math.cos(float(angle_rad_i)))
    $y2_i=float(math.pow(y1_i,2))

    
    $x1_i=200*float(math.sin(float(angle_rad_i)))
    $x2_i=float(math.pow(x1_i,2))

    $x_position_i=400-int(math.sqrt(x2_i))
    $x_i=393-int(math.sqrt(x2_i))
    $y_i=205-int(math.sqrt(y2_i))
    

    show incident:
        rotate 0
        #xanchor 5 yanchor 200
        xpos x_i ypos y_i
        #xpos 400-int(math.sqrt(x2_i)) ypos 300-int(math.sqrt(y2))
        transform_anchor True
        rotate_pad False
        rotate -float(angle_i) 
    
    if angle_i<90:


        "This is the angle of incidence, theta1 \= [theta1] degrees"
        show theta1 labels:
            zoom 0.5
            xpos 59 ypos 60
        show text "{color=#000000}[theta1]{/color}" at theta1_pos as line1
    else:
        "This is total internal reflection, no refraction occurs!"

#--------------------------------------



    "What would you like to do now?"
    hide line1
    hide line2
    hide line3
    hide line4
    hide incident
    hide refracted
    hide test
    hide n1 labels
    hide n2 labels
    hide theta1 labels
    hide theta2 labels
    menu:
        "Calculate same variable":
            jump theta1_calc
        "Calculate a different variable":
            jump calc_choices
        "Quit":
            return
    
    jump start