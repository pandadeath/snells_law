
label theta2_calc:

    "All right, let\'s find the Angle of Refraction."
    hide instructor
    show whiteboard:
        zoom 0.5
        xpos 59 ypos 60  
    
    
    $good_theta1=False
    python:
        while not good_theta1:
            input_incident=renpy.input("Please enter an incident angle in degrees ")
            try:
                incident_angle_number=float(input_incident)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if 0< incident_angle_number <90:
                    "stuff"
                    good_theta1=True
                else:
                    renpy.input("Please put in a number between 0 and 90\nPress Enter to try again.")
    
    $theta1=round(float(input_incident),1)
    $rad_incident=float(input_incident)*math.pi/180
    $y1=200*float(math.cos(float(rad_incident)))
    $y2=float(math.pow(y1,2))
    $y_position_incident=600-int(math.sqrt(y2))-400+(200-int(math.sqrt(y2)))/2
    $x1_i=200*float(math.sin(float(rad_incident)))
    $x2_i=float(math.pow(x1_i,2))
    $x_position_i=400-int(math.sqrt(x2_i))
    $x_i=393-int(math.sqrt(x2_i))
    $y_i=205-int(math.sqrt(y2))
    #"print %(y1)0.2f"
    #"The y position of the upper left corner is %(y_position_incident)0.2f"
    #"the vertical length of the line is %(y1)0.2f"

    show incident:
        rotate 0
        #xanchor 5 yanchor 200
        xpos x_i ypos y_i
        #xpos 400-int(math.sqrt(x2_i)) ypos 300-int(math.sqrt(y2))
        transform_anchor True
        rotate_pad False
        rotate -float(input_incident)
    "This is the angle of incidence, theta1 \= [theta1] degrees"
    show theta1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[theta1]{/color}" at theta1_pos as line1


    "Choose the indices of refraction."
    #$n1=renpy.input("The first index of refraction is;")
   
    $good_n1=False
    python:
        while not good_n1:
            n1=renpy.input("Please enter the incident medium\'s index of refraction")
            try:
                n1_number=float(n1)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if n1_number>=1:
                    "stuff"
                    good_n1=True
                else:
                    renpy.input("Light can\'t go faster than light.\nPut in  a number bigger than 1\nPress Enter to try again.")
    
    
    show n1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[n1]{/color}" at n1_pos as line2
    #$n2=renpy.input("The second index of refraction is:")
    
    $good_n2=False
    python:
        while not good_n2:
            n2=renpy.input("Please enter the refracted medium\'s index of refraction")
            try:
                n2_number=float(n2)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if n2_number>=1:
                    "stuff"
                    good_n2=True
                else:
                    renpy.input("Light can\'t go faster than light.\nPut in  a number bigger than 1\nPress Enter to try again.")
    
    
    show n2 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[n2]{/color}" at n2_pos as line3
    $n_ratio=float(n1)/float(n2)
    $sin_incident=float(math.sin(rad_incident))
    if sin_incident*n_ratio>=1:
        $angle_r=90
        $angle_rad_r=math.pi/2
    else:
        python:
            angle_rad_r=math.asin(n_ratio*sin_incident)
            angle_r=float(angle_rad_r)*180/math.pi
    $theta2=round(float(angle_r),1)
    $y1_r=float(200*math.cos(float(angle_rad_r)))
    $y2_r=float(math.pow(y1_r,2))
    $y_position_refracted=600-400+int(math.sqrt(y2_r))
    $y_position_refracted=y_position_incident+int(math.sqrt(y2))+(200-int(math.sqrt(y2)))/2
    #"the y position of the upper left corner is %(y_position_refracted)0.2f"
    
    $x1_r=200*float(math.sin(float(angle_rad_r)))
    $x2_r=float(math.pow(x1_r,2))
    $x_position_r=x_position_i+int(math.sqrt(x2_i))-(200-int(math.sqrt(x2_i)))/2
    #"show a line at an angle of %(angle_r)0.2f degrees"
    $x_r=400-int(math.sqrt(x2_r))#/2+int(math.sqrt(x2_i))
    $y_r=300#+int(math.sqrt(y2_r))/2+int(math.sqrt(y2))

    #"%(x_r)0.2f"
    show refracted:
        rotate 0
        xanchor 5 yanchor 200
        xpos 405 ypos 210
        
        #xpos 400-int(math.sqrt(x2_i)) ypos 300-int(math.sqrt(y2))
        transform_anchor True
        rotate_pad False
        rotate -float(angle_r)-180

       

    if angle_r<90:
        "The angle of refraction is [angle_r] degrees"
        show theta2 labels:
            zoom 0.5
            xpos 59 ypos 60
        show text "{color=#000000}[theta2]{/color}" at theta2_pos as line4
        show n1 labels:
            zoom 0.5
            xpos 59 ypos 60
    else:
        "This is total internal reflection, no refraction occurs!"

    "What would you like to do now?"
    hide line1
    hide line2
    hide line3
    hide line4
    hide incident
    hide refracted
    hide test
    hide n1 labels
    hide n2 labels
    hide theta1 labels
    hide theta2 labels
    menu:
        "Calculate same variable":
            jump theta2_calc
        "Calculate a different variable":
            jump calc_choices
        "Quit":
            return
    return