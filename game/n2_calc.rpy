
label n2_calc:

    "All right, let\'s find the Index of Refraction for the refracted medium."
    hide instructor
    show whiteboard:
        zoom 0.5
        xpos 59 ypos 60 
    
    $good_theta1=False
    python:
        while not good_theta1:
            input_incident=renpy.input("Please enter an incident angle in degrees ")
            try:
                incident_angle_number=float(input_incident)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if 0< incident_angle_number <90:
                    "stuff"
                    good_theta1=True
                else:
                    renpy.input("Please put in a number between 0 and 90\nPress Enter to try again.")
    
    
    
    $theta1=round(float(input_incident),1)
    $rad_incident=float(input_incident)*math.pi/180
    $y1=200*float(math.cos(float(rad_incident)))
    $y2=float(math.pow(y1,2))
    $y_position_incident=600-int(math.sqrt(y2))-400+(200-int(math.sqrt(y2)))/2
    $x1_i=200*float(math.sin(float(rad_incident)))
    $x2_i=float(math.pow(x1_i,2))
    $x_position_i=400-int(math.sqrt(x2_i))
    $x_i=393-int(math.sqrt(x2_i))
    $y_i=205-int(math.sqrt(y2))
    
    show incident:
        rotate 0
        #xanchor 5 yanchor 200
        xpos x_i ypos y_i
        #xpos 400-int(math.sqrt(x2_i)) ypos 300-int(math.sqrt(y2))
        transform_anchor True
        rotate_pad False
        rotate -float(input_incident)
    "This is the angle of incidence, theta1 \= [theta1] degrees"
    show theta1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[theta1]{/color}" at theta1_pos as line1
    
    
    $good_theta2=False
    python:
        while not good_theta2:
            input_refracted=renpy.input("Please enter an refracted angle in degrees ")
            try:
                refracted_angle_number=float(input_refracted)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if 0< refracted_angle_number <90:
                    "stuff"
                    good_theta2=True
                else:
                    renpy.input("Please put in a number between 0 and 90\nPress Enter to try again.")
    
    
    $theta2=round(float(input_refracted),1)
    $rad_refracted=float(input_refracted)*math.pi/180
    "The Angle of Refraction is theta2=[theta2] degrees."
    
    show refracted:
        rotate 0
        xanchor 5 yanchor 200
        xpos 405 ypos 210
        
        #xpos 400-int(math.sqrt(x2_i)) ypos 300-int(math.sqrt(y2))
        transform_anchor True
        rotate_pad False
        rotate -float(input_refracted)-180

    show theta2 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[theta2]{/color}" at theta2_pos as line4
    
    "Put in the index of refraction for the incident medium."
    #$n1=renpy.input("The incident medium index of refraction is:")
    
    $good_n1=False
    python:
        while not good_n1:
            n1=renpy.input("Please enter the incident medium\'s index of refraction")
            try:
                n1_number=float(n1)

            except ValueError:
                renpy.input("Put in a number.\nPress Enter to try again.")
            else:
                if n1_number>=1:
                    "stuff"
                    good_n1=True
                else:
                    renpy.input("Light can\'t go faster than light.\nPut in  a number bigger than 1\nPress Enter to try again.")
    
    
    show n1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}[n1]{/color}" at n1_pos as line2
    $sin_refracted=float(math.sin(rad_refracted))
    $sin_incident=float(math.sin(rad_incident))
    
    $n2_a=float(n1)*float(sin_incident)/float(sin_refracted)
    $n2=round(float(n2_a),2)
    
    "The index of refraction of the refracted medium is n2=[n2_a]"
    if n2<1:
        "This would mean that light was going faster than the speed of light in the incident medium!\nWhich is not possible!"
        "This means refraction can not occur, instead total internal reflection occured."
    else:
        
        show n2 labels:
            zoom 0.5
            xpos 59 ypos 60
        show text "{color=#000000}[n2]{/color}" at n2_pos as line3
    
    "What would you like to do now?"
    hide line1
    hide line2
    hide line3
    hide line4
    hide n1 labels
    hide n2 labels
    hide theta1 labels
    hide theta2 labels
    hide incident
    hide refracted
    hide test
    menu:
        "Calculate same variable":
            jump n2_calc
        "Calculate a different variable":
            jump calc_choices
        "Quit":
            return
            

