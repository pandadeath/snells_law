﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

# Declare characters used by this game.
#define e = Character('Eileen', color="#c8ffc8")
image bg classroom= "images/classroom01.png"
image whiteboard="images/snells_law_background.png"
image n1 labels="images/snells_law_background_n1.png"
image n2 labels="images/snells_law_background_n2.png"
image theta2 labels="images/snells_law_background_theta2.png"
image theta1 labels="images/snells_law_background_theta1.png"

image whiteboard blank ="images/whiteboard_blank.png"
image snells_law 1 ="images/snells_law01.png"
image snells_law 2 ="images/snells_law02.png"
image snells_law 3 ="images/snells_law03.png"
image snells_law 4 ="images/snells_law04.png"

image example 1 ="images/example01.png"
image example 2 ="images/example02.png"
image example 3 ="images/example03.png"
image example 4 ="images/example04.png"
image example 5 ="images/example05.png"
image example 6 ="images/example06.png"

image instructor 1 ="images/instructor01.png"
image instructor 2 ="images/instructor02.png"
image instructor 3 ="images/instructor03.png"
image instructor 4 ="images/instructor04.png"
image instructor 5 ="images/instructor05.png"

image incident:
    "images/incident01a.png"
    pause 0.25
    "images/incident02a.png"
    pause 0.25
    "images/incident03a.png"
    pause 0.25
    repeat
    
image refracted:
    "images/refracted01.png"
    pause 0.25
    "images/refracted02.png"
    pause 0.25
    "images/refracted03.png"
    pause 0.25
    repeat
    
image test="images/line.png"
init:
    $ import math
    


# The game starts here.
label start:
    $ n1_pos = Position(xanchor = 0.925, xpos = 0.925, yalign = 0.27)
    $ n2_pos = Position(xanchor = 0.41, xpos = 0.41, yalign = 0.43)
    $ theta1_pos = Position(xanchor = 0.67, xpos = 0.67, yalign = 0.15)
    $ theta2_pos = Position(xanchor = 0.38, xpos = 0.38, yalign = 0.72)
    $ instructor_pos1 = Position(xanchor = 0.45, xpos = 0.45, yalign = -0.1)
    $ instructor_pos2 = Position(xanchor = -1.2, xpos = -1.2, yalign = -0.1)
    $ instructor_pos3 = Position(xanchor = -1.5, xpos = -1.5, yalign = -0.1) 
    $ instructor_pos4 = Position(xanchor = -1.0, xpos = -1.0, yalign = -0.1) 
    scene bg classroom
    show instructor 1 at instructor_pos1 with dissolve

    "Welcome to the Snell\'s Law Calculator!"
    "What would you like to do?"
    menu:
        "Go to calculator.":
            jump sad_instructor
        "Go to tutorial.":
            jump tutorial
    label sad_instructor:
        
        show instructor 5 at instructor_pos1 with dissolve
        "OK, fine, you just want the answers to your homework."
        jump calc_choices
        
    label calc_choices:
        show instructor 4 at instructor_pos2 with move


        "Which variable would you like to calculate?"
        menu:
            "Angle of refraction":
                jump theta2_calc
            "Angle of incidence":
                jump theta1_calc
            "The incident index of refraction":
                jump n1_calc
            "The refracted index of refraction":
                jump n2_calc

            #"Index of refraction":
                #jump index_refraction_choices
        #label index_refraction_choices:
            #"Which index of refraction?"
            #menu:
                #"The incident index of refraction":
                    #jump n1_calc

    return
