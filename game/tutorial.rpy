
label tutorial:
    "Good for you, you actually want to learn!"
    hide instructor
    show whiteboard blank:
        zoom 0.5
        xpos 59 ypos 60
    show instructor 1 at instructor_pos1
    show instructor 2 at instructor_pos2 with move

    "Snell's Law decribes what happens to light when it cross from one medium to another."
    show whiteboard:
        zoom 0.5
        xpos 59 ypos 60
    
    "We can start by drawing an incoming ray of light, or the {i}incident{/i} ray."
    $rad_incident=float(30)*math.pi/180
    $y1=200*float(math.cos(float(rad_incident)))
    $y2=float(math.pow(y1,2))
    $y_position_incident=600-int(math.sqrt(y2))-400+(200-int(math.sqrt(y2)))/2
    $x1_i=200*float(math.sin(float(rad_incident)))
    $x2_i=float(math.pow(x1_i,2))
    $x_position_i=400-int(math.sqrt(x2_i))
    $x_i=393-int(math.sqrt(x2_i))
    $y_i=205-int(math.sqrt(y2))
    show incident:
        rotate 0
        xpos x_i ypos y_i
        transform_anchor True
        rotate_pad False
        rotate -30
        
    "This incident ray forms an angle to the vertical dashed line."
    "This angle is called the \"Angle of Incidence\".\nIn this picture the Angle of Incidence is 30.0 degrees"
    show theta1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}30.0{/color}" at theta1_pos as line1
    
    "The horizontal line represents the two different media."
    show instructor 3 with dissolve
    "If the top medium, or the incident medium, is something like space\; the phase velocity of light in space (\~vacuum) is simply the speed of light c."
    "Another way to say this is to say that the {i}index{\i} {i}of{\i} {i}refraction{\i} of the top medium is 1."
    show n1 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}1{/color}" at n1_pos as line2
    "The index of refraction, n, is the ratio of the speed of light in a vacuum to the speed of light in a medium."
    "So that in the case of the top medium, the index of refraction is c/c or n=1"
    "Let\'s now imagine that the bottom medium is water, which happens to have an index of refraction of 1.33"
    show instructor 4 at instructor_pos3 with move
    show n2 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}1.33{/color}" at n2_pos as line3    
    "This means that the phase velocity of light in water is the speed of light in a vacuum (c) divided by 1.33"
    "So the prescence of a medium like water effectively makes the light travel slower."
    "This change in phase velocity of the light from one medium to another is going to be related to the angles the trajectories of the light ray make with respect to a line perpendicular to the interface between the media."
    hide instructor
    show snells_law 1:
        zoom 0.5
        xpos 59 ypos 60
    show instructor 4 at instructor_pos3
    show instructor 3 at instructor_pos2 with move
    "This relation comes from Fermot\'s Principle of Least Time."
    "Light is always going to travel in the manner which will take the least amount of time."
    "In other words, Nature is impatient."
    show snells_law 2:
        zoom 0.5
        xpos 59 ypos 60
    show instructor 2 at instructor_pos3 with move
    "We can use our definition of index of refraction to replace the velocity in the equation."
    "It is important to note here that c is the cosmic speed limit."
    show instructor 4 at instructor_pos3 with dissolve
    "Light can\'t go faster than c, ergo the index of refraction is always bigger than 1"

    show snells_law 3:
        zoom 0.5
        xpos 59 ypos 60
    "Now we put c/n into the Snell\'s Law equation in the place of the velocities."
    show instructor 3 at instructor_pos4 with move
    show snells_law 4:
        zoom 0.5
        xpos 59 ypos 60
    "We then just rearrange it to get the standard form of Snell\'s Law."
    show instructor 2 at instructor_pos4 with dissolve
    "We can cancel out the speed of light c, so our only variables are two angles, and two indices of refraction."
    show instructor 4 at instructor_pos4 with dissolve
    "We then rearrange it so that the two variables relating to medium 1 are on the left, and the two variables relating to medium 2 are on the left."
    hide snells_law
    "We are able to rearrange this equation to solve for any one unkown variable."
    show instructor 2 at instructor_pos4 with dissolve    
    "So in this example, we know the angle of incidence, theta1 =30.0, and the index of refraction for the incident medium, n1=1, and finally the index of refraction of the refracted medium, n2=2"
    "But we need to calculate the Angle of Refraction."
    hide instructor
    show example 1:
        zoom 0.5
        xpos 59 ypos 60
    show instructor 2 at instructor_pos4
    "We start with Snell's Law."
    show instructor 3 at instructor_pos4 with dissolve
    "And we need to isolate the theta2."
    show example 2:
        zoom 0.5
        xpos 59 ypos 60
    "We then divide both sides by n2."
    show instructor 2 at instructor_pos4 with dissolve
    "We have sin(theta2) isolated on one side."
    show example 3:
        zoom 0.5
        xpos 59 ypos 60
    show instructor 4 at instructor_pos4 with dissolve
    show instructor 4 at instructor_pos2 with move    
    "Now we have to isolate theta2 from the inside the argument of the sine function."
    "So we take the inverse sine of both sides of the equation."
    show example 4:
        zoom 0.5
        xpos 59 ypos 60
    "We can cancel out inverse sine of sine."
    show example 5:
        zoom 0.5
        xpos 59 ypos 60
    show instructor 4 at instructor_pos3 with move 
    "And now we have theta2 isolated on one side and can plug in numbers."
    show example 5:
        zoom 0.5
        xpos 59 ypos 60
    "Since this is just \"plug and chug\", it is easy to write a little program to do it."
    "So let\'s see what the answer is."
    hide example
    "Here it goes..."
    show instructor 2 at instructor_pos3 with dissolve 
    show refracted:
        rotate 0
        xanchor 5 yanchor 200
        xpos 405 ypos 210
        transform_anchor True
        rotate_pad False
        rotate -float(22.1)-180
    show instructor 2 at instructor_pos4 with move 
    "There is our light ray after it has been refracted by the change in media."
    show instructor 2 at instructor_pos3 with move     
    show theta2 labels:
        zoom 0.5
        xpos 59 ypos 60
    show text "{color=#000000}22.1{/color}" at theta2_pos as line4
    "And there\'s the answer, the angle of refraction, theta2 = 22.1 degrees." 
    "And you are done!"
    hide instructor
    show instructor 2 at instructor_pos3
    show instructor 2 at instructor_pos1 with move
    show instructor 1 at instructor_pos1 with dissolve 
    "You sat through the whole tutorial, you can feel good about yourself and go use the calulator guilt free!"
    hide instructor
    jump start
